import { IParametersModel } from "../services/parameters/parameters.service";

export interface ISelectContent {
  id: string;
  value: string;
  isChecked: boolean;
}

export const addCurrentFilterCheckboxState = (
  currentFilterValues: IParametersModel[],
  currentParameterState: string[]
): ISelectContent[] => {
  const initialValue = currentFilterValues.map((el) => {
    if (currentParameterState.includes(el.id)) {
      return { ...el, isChecked: true };
    }
    return { ...el, isChecked: false };
  });

  return initialValue;
};
