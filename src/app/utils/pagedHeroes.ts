import { IHeroes } from "../data/heroes";

export const getMappedHeroes = (arr: IHeroes[]): Map<number, IHeroes[]> => {
  let key: number = 0;
  const third: number = 3;
  const maxKey = 5;

  const heroesMap = arr.reduce((acc: Map<number, IHeroes[]>, current: IHeroes, index: number) => {
    if (index % third === 0) {
      key++;
    }
    if (key > maxKey) {
      return acc;
    }
    const value = acc.get(key);
    if (value === undefined) {
      acc.set(key, [current]);
    } else {
      acc.set(key, [...value, current]);
    }

    return acc;
  }, new Map());

  return heroesMap;
};
