import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  public imageLogo = '../assets/images/logo.png';
  public linksText = ['Home', 'Preview'];
}
