export interface IHeroes {
  id: string;
  name: string;
  imageURL: string;
  gender: string;
  race: string;
  side: string;
  nameColor: string;
  backgroundColor: string;
  parametersColor: string;
}

export const heroes: IHeroes[] = [
  {
    id: '1',
    name: 'Yoda',
    imageURL:
      'https://starwarsblog.starwars.com/wp-content/uploads/2015/11/yoda-the-empire-strikes-back-1536x864-349144518002.jpg',
    gender: 'Man',
    race: 'Undefined',
    side: 'Light',
    nameColor: '#ffffff',
    backgroundColor: '#000000',
    parametersColor: '#ffffff',
  },
  {
    id: '2',
    name: 'Rey',
    imageURL:
      'https://i.insider.com/5dfbe287954bda1494218216?width=1136&format=jpeg',
    gender: 'Woman',
    race: 'Human',
    side: 'Light',
    nameColor: '#ffffff',
    backgroundColor: '#000000',
    parametersColor: '#ffffff',
  },
  {
    id: '3',
    name: 'Chewbacca',
    imageURL:
      'https://thumbor.forbes.com/thumbor/fit-in/1200x0/filters%3Aformat%28jpg%29/https%3A%2F%2Fspecials-images.forbesimg.com%2Fdam%2Fimageserve%2F958761228%2F0x0.jpg%3Ffit%3Dscale',
    gender: 'Man',
    race: 'Wookiee',
    side: 'Light',
    nameColor: '#ffffff',
    backgroundColor: '#000000',
    parametersColor: '#ffffff',
  },
  {
    id: '4',
    name: 'Yoda',
    imageURL:
      'https://starwarsblog.starwars.com/wp-content/uploads/2015/11/yoda-the-empire-strikes-back-1536x864-349144518002.jpg',
    gender: 'Man',
    race: 'Undefined',
    side: 'Light',
    nameColor: '#ffffff',
    backgroundColor: '#000000',
    parametersColor: '#ffffff',
  },
  {
    id: '5',
    name: 'Rey',
    imageURL:
      'https://i.insider.com/5dfbe287954bda1494218216?width=1136&format=jpeg',
    gender: 'Woman',
    race: 'Human',
    side: 'Light',
    nameColor: '#ffffff',
    backgroundColor: '#000000',
    parametersColor: '#ffffff',
  },
  {
    id: '6',
    name: 'Yoda',
    imageURL:
      'https://starwarsblog.starwars.com/wp-content/uploads/2015/11/yoda-the-empire-strikes-back-1536x864-349144518002.jpg',
    gender: 'Man',
    race: 'Undefined',
    side: 'Light',
    nameColor: '#ffffff',
    backgroundColor: '#000000',
    parametersColor: '#ffffff',
  },
  {
    id: '7',
    name: 'Chewbacca',
    imageURL:
      'https://thumbor.forbes.com/thumbor/fit-in/1200x0/filters%3Aformat%28jpg%29/https%3A%2F%2Fspecials-images.forbesimg.com%2Fdam%2Fimageserve%2F958761228%2F0x0.jpg%3Ffit%3Dscale',
    gender: 'Man',
    race: 'Wookiee',
    side: 'Light',
    nameColor: '#ffffff',
    backgroundColor: '#000000',
    parametersColor: '#ffffff',
  },
  {
    id: '8',
    name: 'Rey',
    imageURL:
      'https://i.insider.com/5dfbe287954bda1494218216?width=1136&format=jpeg',
    gender: 'Woman',
    race: 'Human',
    side: 'Light',
    nameColor: '#ffffff',
    backgroundColor: '#000000',
    parametersColor: '#ffffff',
  },
  {
    id: '9',
    name: 'Yoda',
    imageURL:
      'https://starwarsblog.starwars.com/wp-content/uploads/2015/11/yoda-the-empire-strikes-back-1536x864-349144518002.jpg',
    gender: 'Man',
    race: 'Undefined',
    side: 'Light',
    nameColor: '#ffffff',
    backgroundColor: '#000000',
    parametersColor: '#ffffff',
  },
  {
    id: '10',
    name: 'Chewbacca',
    imageURL:
      'https://thumbor.forbes.com/thumbor/fit-in/1200x0/filters%3Aformat%28jpg%29/https%3A%2F%2Fspecials-images.forbesimg.com%2Fdam%2Fimageserve%2F958761228%2F0x0.jpg%3Ffit%3Dscale',
    gender: 'Man',
    race: 'Wookiee',
    side: 'Light',
    nameColor: '#ffffff',
    backgroundColor: '#000000',
    parametersColor: '#ffffff',
  },
];
