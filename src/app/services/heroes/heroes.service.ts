import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IHeroes } from 'src/app/data/heroes';
import { HttpService } from '../http/http.service';
import { IFiltersData } from 'src/app/core/pages/preview/preview.component';
import { IParametersModel } from '../parameters/parameters.service';

export interface IDataResponse {
  content: Array<IHeroes>;
  first: true;
  last: true;
  number: number;
  numberOfElements: number;
  size: number;
  totalElements: number;
  totalPages: number;
}

export interface IHeroCardModel {
  id?: string;
  name: string;
  description: string;
  imageURL: string;
  nameColor: string;
  backgroundColor: string;
  parametersColor: string;
  tag1: string;
  tag2: string;
  tag3: string;
  gender: IParametersModel;
  race: IParametersModel;
  side: IParametersModel;
}

@Injectable()
export class HeroesService {
  constructor(private HttpService: HttpService) {}

  public getHeroes(filtersState?: IFiltersData): Observable<IDataResponse> {
    if (filtersState  && Object.values(filtersState).filter((el: string[]) => el.length !== 0).length !== 0) {
      const url: string =
        `http://localhost:5000/api/STAR_WARS/character?values=${filtersState.search.join(
          '&values='
        )}&gender=${filtersState.gender.join('&gender=')}&race=${filtersState.race.join(
          '&race='
        )}&side=${filtersState.side.join('&side=')}&page=0&size=15`;
      return this.HttpService.get<IDataResponse>(url);
    } else {
      const url: string =
        'http://localhost:5000/api/STAR_WARS/character?page=0&size=15';
      return this.HttpService.get<IDataResponse>(url);
    }
  }

  public getCurrentHero(id: string) {
    const url = `http://localhost:5000/api/STAR_WARS/character/${id}`
    return this.HttpService.get<IHeroCardModel>(url)
  }
}
