import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpService } from './../http/http.service';

export interface IParametersModel {
  id: string;
  value: string;
}

@Injectable()
export class ParametersService {
  constructor(private HttpService: HttpService) {}

  public getCurrentParameters(parameterName: string): Observable<Array<IParametersModel>> {
    const url = `http://localhost:5000/api/STAR_WARS/${parameterName}`
    return this.HttpService.get<Array<IParametersModel>>(url)
  }
}
