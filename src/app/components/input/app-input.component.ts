import { Component, Input } from "@angular/core";

@Component({
  selector: 'app-input',
  templateUrl: './app-input.component.html',
  styleUrls: ['./app-input.component.css']
})
export class InputComponent {
  @Input() public inputId: string;
  @Input() public name: string;
  @Input() public icon?: string;
  @Input() public value?: string;
  @Input() public disabled?: boolean;
  @Input() public placeholder: string;
  @Input() public inputStyle: { [key: string]: any; } | null;
  @Input() public containerStyle: { [key: string]: any; } | null;
  @Input() public iconStyle: { [key: string]: any; } | null;
}
