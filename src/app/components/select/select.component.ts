import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
})
export class SelectComponent implements OnInit, OnDestroy {
  @Input() dropdownId: string;
  @Input() isDropdownOpen: boolean;
  public fromClick: Subscription;

  public openCloseDropdown($event: any) {
    if ($event.target.id === this.dropdownId) {
      this.isDropdownOpen = !this.isDropdownOpen;
    }
  }

  public fromDOMEvent(element: HTMLElement, type: string): Observable<Event> {
    return new Observable((subscribe) => {
      element.addEventListener(type, (event: Event) => subscribe.next(event));
    });
  }

  public clickOut = (e: Event): void => {
    const target = e.target as HTMLDivElement;
    if (
      !target.closest(`#dropdown_${this.dropdownId}`) &&
      !target.closest(`#${this.dropdownId}`)
    ) {
      this.isDropdownOpen = false;
    }
  };

  ngOnInit(): void {
    this.fromClick = this.fromDOMEvent(document.body, 'click').subscribe(
      (event) => {
        this.clickOut(event);
      }
    );
  }

  ngOnDestroy(): void {
    this.fromClick.unsubscribe();
  }
}
