import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-input-color',
  templateUrl: './input-color.component.html',
  styleUrls: ['./input-color.component.css'],
})
export class InputColorComponent {
  @Input() public inputId: string;
  @Input() public name: string;
  @Input() public value: string;
}
