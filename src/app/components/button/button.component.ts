import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css'],
})
export class ButtonComponent {
  constructor() {};
  @Input()
  public text: string;
  @Input()
  public disabled: boolean;

  @Input()
  public textFontSize: number;

  @Input()
  public width: number;
  @Input()
  public height: number;
}
