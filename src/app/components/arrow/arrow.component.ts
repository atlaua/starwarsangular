import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-arrow',
  templateUrl: './arrow.component.html',
})
export class ArrowComponent {
  @Input()
  public arrowForwardIcon: string;

  @Input()
  public isForwardArrow: boolean;
}
