import { Component, Input } from "@angular/core";

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css']
})
export class CheckboxComponent {
  @Input() public id: string;
  @Input() public inputName: string;
  @Input() public isChecked: boolean;
  @Input() public label: string;
}
