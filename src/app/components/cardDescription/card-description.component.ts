import { Component, Input } from "@angular/core";
import { IHeroes } from "src/app/data/heroes";
import { IHeroCardModel } from "src/app/services/heroes/heroes.service";

export interface IHeroDescription {
  gender: string;
  race: string;
  side: string;
  backgroundColor: string;
  parametersColor: string;
}

@Component({
  selector: 'app-card-description',
  templateUrl: './card-description.component.html',
  styleUrls: ['./card-description.component.css'],
})
export class CardDescription {
  @Input() gender: string;
  @Input() race: string;
  @Input() side: string;
  @Input() public descriptionStyle: { [key: string]: any; } | null;
}
