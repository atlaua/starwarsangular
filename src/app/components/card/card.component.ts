import { Component, Input } from "@angular/core";
import { IHeroes } from './../../data/heroes';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent {
  @Input() hero: IHeroes;

  public nameColor(): string {
    return this.hero.nameColor ? this.hero.nameColor : 'white';
  }
}
