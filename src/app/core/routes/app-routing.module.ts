import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeroModalComponent } from '../heroModal/heroModal.component';
import { HomeComponent } from '../pages/home/home.component';
import { PreviewComponent } from '../pages/preview/preview.component';

export const routes: Routes = [
  {
    path: "",
    component: HomeComponent
  },
  {
    path: "preview",
    component: PreviewComponent,
    pathMatch: 'full'
  },
  {
    path: "preview/:id",
    component: PreviewComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
