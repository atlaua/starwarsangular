import {
  Component,
  EventEmitter,
  Input,
  Output,
  OnInit,
  OnChanges,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css'],
})
export class PaginationComponent implements OnInit, OnChanges {
  public arrowIcon = '../../../assets/images/arrowForward.png';
  @Input() public changeElement: number;
  @Input() public totalElements: number;
  @Input() public shownElementsOnPage: number;
  public arrayDots: number[] = [];

  @Output() public pageChanged: EventEmitter<number> =
    new EventEmitter<number>();

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.totalElements) {
      this.fillArrayDots();
    }
  }

  public onClickBackArrow() {
    if (this.changeElement >= 2) {
      this.pageChanged.emit(--this.changeElement);
    } else return;
  }

  public onClickForwardArrow() {
    if (this.changeElement < this.totalElements) {
      this.pageChanged.emit(++this.changeElement);
    } else return;
  }

  public toggleActiveDot(): number {
    if (this.changeElement === 1) {
      //закрасить первую точку
      return 0;
    } else if (
      this.changeElement ===
      Math.ceil(this.totalElements / this.shownElementsOnPage)
    ) {
      // закрасить последнюю точку
      return this.arrayDots.length - 1;
    } else {
      // красить среднюю точку
      return 1;
    }
  }

  public addActiveClass(el: number) {
    return this.toggleActiveDot() === el ? 'activePage' : '';
  }

  public fillArrayDots() {
    const dotsCount = Math.ceil(this.totalElements / this.shownElementsOnPage);
    const maxDots: number = 3;
    this.arrayDots = [];
    for (let i = 0; i < dotsCount; i++) {
      if (i < maxDots) {
        this.arrayDots = [...this.arrayDots, i];
      }
    }
  }

  ngOnInit() {
    this.fillArrayDots();
  }
}
