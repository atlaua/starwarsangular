import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ArrowComponent } from 'src/app/components/arrow/arrow.component';
import { CardComponent } from 'src/app/components/card/card.component';
import { CardDescription } from 'src/app/components/cardDescription/card-description.component';
import { PreviewComponent } from './preview.component';
import { PaginationComponent } from '../../pagination/pagination.component';
import { InputComponent } from 'src/app/components/input/app-input.component';
import { FormFilterComponent } from '../../formFilters/app-form-filters.component';
import { DropdownComponent } from 'src/app/components/dropdown/dropdown.component';
import { CheckboxComponent } from 'src/app/components/checkbox/checkbox.component';
import { Loader } from 'src/app/components/loader/loader.component';
import { SelectComponent } from 'src/app/components/select/select.component';
import { FilterSelectComponent } from '../../formFilters/filterSelect/filter-select.component';
import { HeroModalComponent } from '../../heroModal/heroModal.component';
import { ModalContainerComponent } from 'src/app/components/modalContainer/modal-container.component';
import { AddHeroComponent } from '../../addHeroForm/add-hero.component';
import { InputColorComponent } from 'src/app/components/inputColor/input-color.component';
import { ButtonSharedModule } from '../buttonSharedModel/button-shared.module';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    PreviewComponent,
    CardComponent,
    CardDescription,
    ModalContainerComponent,
    HeroModalComponent,
    AddHeroComponent,
    InputColorComponent,
    ArrowComponent,
    PaginationComponent,
    InputComponent,
    SelectComponent,
    FormFilterComponent,
    DropdownComponent,
    CheckboxComponent,
    FilterSelectComponent,
    Loader,
  ],
  imports: [CommonModule, FormsModule, ButtonSharedModule],
})
export class PreviewModule {
  constructor() {};
}
