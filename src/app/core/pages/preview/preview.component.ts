import { Component, OnInit } from '@angular/core';
import { IHeroes } from 'src/app/data/heroes';
import { getMappedHeroes } from 'src/app/utils/pagedHeroes';
import {
  HeroesService,
  IHeroCardModel,
} from 'src/app/services/heroes/heroes.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ISelectContent } from '../../formFilters/app-form-filters.component';
import { ParametersService } from 'src/app/services/parameters/parameters.service';

export interface IFiltersData {
  gender: string[];
  race: string[];
  side: string[];
  search: string[];
}

export interface IDataResponse {
  content: IHeroes[];
  first: true;
  last: true;
  number: number;
  numberOfElements: number;
  size: number;
  totalElements: number;
  totalPages: number;
}

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.css'],
  providers: [HeroesService, ParametersService],
})
export class PreviewComponent implements OnInit {
  public filters: string;
  public cardId: string;
  public iconAdd = '../../../../assets/images/addPlus.png';
  public currentPage = 1;
  public heroes: IHeroes[];
  public heroesMap: Map<number, Array<IHeroes>>;
  public filtersState: IFiltersData = {
    gender: [],
    race: [],
    side: [],
    search: [],
  };
  public currentHero: IHeroCardModel;
  public showHeroModal = false;
  public showAddModal = false;

  constructor(
    private HeroesService: HeroesService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {
    this.filters = this.activatedRoute.snapshot.queryParams['filters'];
    if (this.activatedRoute.snapshot.params['id']) {
      this.cardId = this.activatedRoute.snapshot.params['id'];
      this.openCard(this.cardId);
    }
  }

  public getCurrentCards() {
    return this.heroesMap?.get(this.currentPage);
  }

  public openCard(id: string) {
    this.router.navigate(['/preview', id]);
    this.HeroesService.getCurrentHero(id).subscribe((result) => {
      this.currentHero = result;
      this.showHeroModal = true;
    });
  }

  public openAddForm() {
    this.showAddModal = true;
  }

  public closeModal(event: any) {
    const idModal = event.target.id;
    const idBtn = event.target.id;
    this.router.navigate(['/preview']);
    if (idModal === 'heroModal' || idBtn === 'heroModalClose') {
      this.showHeroModal = false;
    } else if (idModal === 'addModal' || idBtn === 'addModalClose') {
      this.showAddModal = false;
    }
  }

  public filtersStateChanged($event: IFiltersData) {
    this.filtersState = $event;
    if (
      Object.values(this.filtersState).filter((el: string[]) => el.length !== 0)
        .length === 0
    ) {
      this.router.navigate(['/preview'], {});
      this.HeroesService.getHeroes().subscribe((result) => {
        this.heroes = result.content;
        this.heroesMap = getMappedHeroes(this.heroes);
      });
    } else {
      this.router.navigate(['.'], {
        relativeTo: this.activatedRoute,
        queryParams: { filters: JSON.stringify(this.filtersState) },
      });
      this.HeroesService.getHeroes(this.filtersState).subscribe((result) => {
        this.heroes = result.content;
        this.heroesMap = getMappedHeroes(this.heroes);
      });
    }
  }

  public pageChangedInRegistry($event: number) {
    this.currentPage = $event;
  }

  public isLoading: boolean = false;
  ngOnInit(): void {
    if (this.filters) {
      this.filtersState = JSON.parse(this.filters);
      this.HeroesService.getHeroes(this.filtersState).subscribe((result) => {
        this.heroes = result.content;
        this.heroesMap = getMappedHeroes(this.heroes);
        this.isLoading = true;
      });
    } else {
      this.HeroesService.getHeroes().subscribe((result) => {
        this.heroes = result.content;
        this.heroesMap = getMappedHeroes(this.heroes);
        this.isLoading = true;
      });
    }
  }
}
