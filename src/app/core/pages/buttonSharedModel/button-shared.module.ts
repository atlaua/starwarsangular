import { NgModule } from '@angular/core';
import { ButtonComponent } from 'src/app/components/button/button.component';

@NgModule({
  declarations: [ButtonComponent],
  exports: [ButtonComponent]
})
export class ButtonSharedModule {
  constructor() {};
}
