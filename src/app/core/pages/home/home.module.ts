import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ButtonSharedModule } from '../buttonSharedModel/button-shared.module';
import { HomeComponent } from './home.component';

@NgModule({
  declarations: [HomeComponent],
  imports: [ButtonSharedModule, CommonModule],
})
export class HomeModule {
  constructor() {};
}
