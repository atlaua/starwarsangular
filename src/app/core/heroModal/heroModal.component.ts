import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IHeroCardModel } from 'src/app/services/heroes/heroes.service';

@Component({
  selector: 'app-heroModal',
  templateUrl: './heroModal.component.html',
  styleUrls: ['./heroModal.component.css'],
})
export class HeroModalComponent {
  @Input() currentHero: IHeroCardModel;

  onClick($event: any) {
    $event.stopPropagation();
  }
}
