import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { IHeroCardModel } from 'src/app/services/heroes/heroes.service';
import {
  IParametersModel,
  ParametersService,
} from 'src/app/services/parameters/parameters.service';

@Component({
  selector: 'app-add-hero',
  templateUrl: './add-hero.component.html',
  styleUrls: ['./add-hero.component.css'],
})
export class AddHeroComponent implements OnInit {
  constructor(private ParametersService: ParametersService) {}
  public isDropdownOpen: boolean = false;
  public views: Array<string> = ['View1', 'View2'];
  public viewingWindow: number = 1;
  public addingHero: IHeroCardModel = {
    name: '',
    description: '',
    imageURL: '',
    nameColor: '',
    backgroundColor: '',
    parametersColor: '',
    tag1: '',
    tag2: '',
    tag3: '',
    gender: {
      id: '',
      value: '',
    },
    race: {
      id: '',
      value: '',
    },
    side: {
      id: '',
      value: '',
    },
  };

  public genders: IParametersModel[];
  public races: IParametersModel[];
  public sides: IParametersModel[];

  public tags = '';
  public arrayTags: Array<string> = [];
  public maxLengthTags: number = 3;

  public viewChanged($event: any) {
    this.viewingWindow = $event;
  }

  public getTags() {
    this.arrayTags = this.tags.split(',');
    for (let i = 0; i < this.arrayTags.length; i++) {
      this.addingHero.tag1 = this.arrayTags[0];
      this.addingHero.tag2 = this.arrayTags[1];
      this.addingHero.tag3 = this.arrayTags[2];
    }
    console.log(this.addingHero);
  }

  public changeCheckbox($event: any) {
    console.log($event.target)
  }

  public isLoading = false;
  ngOnInit(): void {
    const observable$ = forkJoin([
      this.ParametersService.getCurrentParameters('gender'),
      this.ParametersService.getCurrentParameters('race'),
      this.ParametersService.getCurrentParameters('side'),
    ]);

    observable$.subscribe(([resultGender, resultRace, resultSide]) => {
      this.genders = resultGender;
      this.races = resultRace;
      this.sides = resultSide;
      this.isLoading = true;
    });
  }
}
