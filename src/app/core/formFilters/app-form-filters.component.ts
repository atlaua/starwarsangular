import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { forkJoin } from 'rxjs';
import { ParametersService } from 'src/app/services/parameters/parameters.service';
import { addCurrentFilterCheckboxState } from 'src/app/utils/addCurrentFilterCheckboxState';
import { IFiltersData } from '../pages/preview/preview.component';
import { ICurrentFilterState } from './filterSelect/filter-select.component';

export interface ISelectContent {
  id: string;
  value: string;
  isChecked: boolean;
}

@Component({
  selector: 'app-form-filters',
  templateUrl: './app-form-filters.component.html',
  styleUrls: ['./app-form-filters.component.css'],
})
export class FormFilterComponent implements OnInit {
  constructor(private ParametersService: ParametersService) {}

  @Input() public filtersState: IFiltersData;

  public iconSearch = '../../../../assets/images/search-icon.png';
  public iconArrow = '../../../../assets/images/arrowDownSVG.svg';
  public inputStyleSearch = { height: '50px' };
  public iconStyleSearch = { top: '14px', right: '17px' };
  public inputStyleFilter = { width: '140px', height: '45px' };
  public iconStyleFilter = { top: '19px', left: '115px' };

  public initialValuesGender: ISelectContent[];
  public initialValuesRace: ISelectContent[];
  public initialValuesSide: ISelectContent[];

  public genderId: string = 'gender';
  public raceId: string = 'race';
  public sideId: string = 'side';

  public searchValue: string;

  @Output() filtersValueChanged: EventEmitter<IFiltersData> =
    new EventEmitter<IFiltersData>();

  public selectFiltersChange($event: ICurrentFilterState) {
    if ($event.filterName === 'Gender') {
      this.filtersState.gender = $event.valueIds;
      this.filtersValueChanged.emit(Object.assign({}, this.filtersState));
    } else if ($event.filterName === 'Race') {
      this.filtersState.race = $event.valueIds;
      this.filtersValueChanged.emit(Object.assign({}, this.filtersState));
    } else if ($event.filterName === 'Side') {
      this.filtersState.side = $event.valueIds;
      this.filtersValueChanged.emit(Object.assign({}, this.filtersState));
    }
  }

  public searchInput() {
    if (this.searchValue !== '') {
      this.filtersState.search = this.searchValue.split(' ');
      this.filtersValueChanged.emit(Object.assign({}, this.filtersState));
    } else {
      this.filtersState.search = [];
      this.filtersValueChanged.emit(Object.assign({}, this.filtersState));
    }
  }

  public isLoading = false;

  ngOnInit(): void {
    const observable$ = forkJoin([
      this.ParametersService.getCurrentParameters('gender'),
      this.ParametersService.getCurrentParameters('race'),
      this.ParametersService.getCurrentParameters('side'),
    ]);

    observable$.subscribe(([resultGender, resultRace, resultSide]) => {
      this.initialValuesGender = addCurrentFilterCheckboxState(
        resultGender,
        this.filtersState.gender
      );
      this.initialValuesRace = addCurrentFilterCheckboxState(
        resultRace,
        this.filtersState.race
      );
      this.initialValuesSide = addCurrentFilterCheckboxState(
        resultSide,
        this.filtersState.side
      );
      this.isLoading = true;
    });

    if (this.filtersState.search.length > 0) {
      this.searchValue = this.filtersState.search.join(' ');
    } else {
      this.searchValue = '';
    }
  }
}
