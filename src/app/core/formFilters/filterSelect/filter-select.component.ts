import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

interface ISelectContent {
  id: string;
  value: string;
  isChecked: boolean;
}

export interface ICurrentFilterState {
  filterName: string;
  valueIds: string[];
}

@Component({
  selector: 'app-filter-select',
  templateUrl: './filter-select.component.html',
})
export class FilterSelectComponent implements OnInit {
  public currentFilterValue: string;
  public isDropdownOpen: boolean = false;

  @Input() public dropdownId: string;

  @Input() public inputId: string;
  @Input() public inputName: string;

  @Input() public initialValues: ISelectContent[];
  @Input() public filterName: string;

  @Input() public icon: string;
  @Input() public inputStyle: { [key: string]: any } | null;
  @Input() public iconStyle: { [key: string]: any } | null;

  public currentFilterState: ICurrentFilterState = {
    filterName: '',
    valueIds: [],
  };

  @Output() currentFilterChanged: EventEmitter<ICurrentFilterState> =
    new EventEmitter<ICurrentFilterState>();

  public changeCurrentFilterValue() {
    if (this.currentFilterState.valueIds.length === 0) {
      this.currentFilterValue = this.filterName;
    } else {
      this.currentFilterValue = `${this.filterName}: ${this.currentFilterState.valueIds.length}`;
    }
  }

  public changeCheckbox($event: any) {
    if ($event.target.checked === true) {
      this.currentFilterState.valueIds = [
        ...this.currentFilterState.valueIds,
        $event.target.id,
      ];
      this.currentFilterState = {
        filterName: this.filterName,
        valueIds: this.currentFilterState.valueIds,
      };
      this.currentFilterChanged.emit(
        Object.assign({}, this.currentFilterState)
      );
      this.changeCurrentFilterValue();
    } else {
      this.currentFilterState.valueIds = [
        ...this.currentFilterState.valueIds.filter(
          (el) => el !== $event.target.id
        ),
      ];
      this.currentFilterState = {
        filterName: this.filterName,
        valueIds: this.currentFilterState.valueIds,
      };
      this.currentFilterChanged.emit(
        Object.assign({}, this.currentFilterState)
      );
      this.changeCurrentFilterValue();
    }
  }

  ngOnInit(): void {
    this.initialValues.map((el) => {
      if (el.isChecked) {
        this.currentFilterState = {
          filterName: this.filterName,
          valueIds: [...this.currentFilterState.valueIds, el.id],
        };
      }
    });
    this.changeCurrentFilterValue();
  }
}
